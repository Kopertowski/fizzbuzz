package FizzBuzz;

public class FizzBuzzPrinter {
    static final int MAX = 100;
    public void print() {
        boolean stringPrinted;
        for (int i=1; i <= MAX; i++) {
            stringPrinted = false;
            if ((i % 3) == 0) {
                System.out.print("Fizz");
                stringPrinted = true;
            }
            if ((i % 5) == 0) {
                System.out.print("Buzz");
                stringPrinted = true;
            }
            if (stringPrinted == false) {
                System.out.print(i);
            }
            System.out.print("\n");
        }
    }
}
